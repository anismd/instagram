package com.krdbootcamp.instagram.user;

import com.krdbootcamp.instagram.common.exception.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements IUserService{
    private final UserRepository repository;

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public User update(User user) {
       User lastSaveUser=findById(user.getId());
       lastSaveUser.setFirstName(user.getFirstName());
       lastSaveUser.setLastName(user.getLastName());
       lastSaveUser.setBio(user.getBio());
       lastSaveUser.setEmail(user.getBio());
       lastSaveUser.setPassword(user.getPassword());
       lastSaveUser.setPhone(user.getPhone());
       lastSaveUser.setProfileImage(user.getProfileImage());
       lastSaveUser.setUserName(user.getUserName());
       lastSaveUser.setPostCount(user.getPostCount());
       lastSaveUser.setFollowerCount(user.getFollowerCount());
       lastSaveUser.setFollowingCount(user.getFollowingCount());
       return repository.save(lastSaveUser);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);

    }

    @Override
    public User findById(Long id) {
        Optional<User> optionalUser=repository.findById(id);
        if (!optionalUser.isPresent()){
            throw  new NotFoundException("Not Found User");
        }
        return  optionalUser.get();
    }

    @Override
    public Page<User> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page,size));
    }



    //for vaadin test
    @Override
    public List<User> getAll() {
        return (List<User>) repository.findAll();
    }

}
