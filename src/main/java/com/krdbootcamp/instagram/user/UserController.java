package com.krdbootcamp.instagram.user;

import com.krdbootcamp.instagram.common.PagingData;
import com.krdbootcamp.instagram.follower.Follower;
import com.krdbootcamp.instagram.follower.FollowerDTO;
import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api-userController")
@AllArgsConstructor
public class UserController {
    private final UserMapper mapper;
    private final IUserService iUserService;

    @PostMapping
    public ResponseEntity<UserDTO> save(@RequestBody UserDTO userDTO){
        User user=mapper.toUser(userDTO);
        iUserService.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    public ResponseEntity<UserDTO> update(@RequestBody UserDTO userDTO){
        User user=mapper.toUser(userDTO);
        iUserService.update(user);
        return ResponseEntity.ok(userDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        iUserService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable Long id){
        User user=iUserService.findById(id);
        UserDTO userDTO=mapper.toUserDTO(user);
        return ResponseEntity.ok(userDTO);
    }

    @Timed("user.getAll")
    @GetMapping("/{page}/{size}")
    public ResponseEntity<PagingData<UserDTO>> getAll(@PathVariable Integer page, @PathVariable Integer size){
        Page<User> userPage=iUserService.findAll(page,size);
        int total=userPage.getTotalPages();
        List<User> users=userPage.getContent();
        List<UserDTO> userDTOS=mapper.toUserDTOS(users);
        PagingData<UserDTO> userDTOPagingData=new PagingData<>(total,page,userDTOS);
        return ResponseEntity.ok(userDTOPagingData);
    }


    //for vaadin test
    @GetMapping
    public  ResponseEntity<List<UserDTO>> getAll(){
        List<User> userList=iUserService.getAll();
        List<UserDTO> userDTOS=mapper.toUserDTOS(userList);
        return ResponseEntity.ok(userDTOS);
    }
}

