package com.krdbootcamp.instagram.user;

import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "Spring")
public interface UserMapper {

    User toUser(UserDTO userDTO);
    UserDTO toUserDTO(User user);
    List<User> toUsers(List<UserDTO> userDTOS);
    List<UserDTO> toUserDTOS(List<User> users);
}
