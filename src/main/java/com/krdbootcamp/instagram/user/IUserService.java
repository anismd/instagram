package com.krdbootcamp.instagram.user;

import org.springframework.data.domain.Page;

import java.util.List;


public interface IUserService {


    User save(User user);
    User update(User user);
    void delete(Long id);
    User findById (Long id);
    Page<User> findAll(Integer page, Integer size);

    //for vaadin test
    List<User> getAll();

}
