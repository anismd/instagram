package com.krdbootcamp.instagram.user;

import com.krdbootcamp.instagram.comment.Comment;
import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.follower.Follower;
import com.krdbootcamp.instagram.following.Following;
import com.krdbootcamp.instagram.like.Like;
import com.krdbootcamp.instagram.post.Post;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Audited
@Table(name = "tbl_user")
@Entity
public class User extends BaseEntity {

    @NotNull
    @Column(name = "firs_name")
    private  String firstName;

    @NotNull
    @Column(name = "last_name")
    private  String lastName;

    @NotNull
    @Column(name = "email")
    private  String email;

    @NotNull
    @Column(name = "phone")
    private  String phone;

    @NotNull
    @Column(name = "password")
    private  String password;

    @NotNull
    @Column(name = "user_name")
    private  String userName;

    @NotNull
    @Column(name = "bio")
    private  String bio;

    @NotNull
    @Column(name = "profile_image")
    private  String profileImage;

    @NotNull
    @Column(name = "post_count")
    private Long postCount;

    @NotNull
    @Column(name = "follower_count")
    private Long followerCount;

    @NotNull
    @Column(name = "following_count")
    private Long followingCount;



    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Comment> comments;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Like> likes;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Follower> followers;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Following> followings;
}
