package com.krdbootcamp.instagram.common.exception;

public class NotFoundException  extends RuntimeException {

    public NotFoundException(String exception) {
        super(exception);
    }
}
