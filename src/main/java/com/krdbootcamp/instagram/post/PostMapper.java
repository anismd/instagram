package com.krdbootcamp.instagram.post;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface PostMapper {

    Post toPost(PostDTO postDTO);
    PostDTO toPostDTO(Post post);
    List<Post> toPosts(List<PostDTO> postDTOS);
    List<PostDTO> toPostDTOS(List<Post> posts);
}
