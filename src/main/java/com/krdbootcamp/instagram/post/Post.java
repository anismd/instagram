package com.krdbootcamp.instagram.post;

import com.krdbootcamp.instagram.comment.Comment;
import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.like.Like;
import com.krdbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Audited
@Table(name = "tbl_post")
@Entity
public class Post extends BaseEntity {


    @NotNull
    private String title;

    @NotNull
    @Column(name = "post_link")
    private String postLink;

    @NotNull
    private String description;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "post",cascade = CascadeType.ALL)
    List<Like> likes;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "post",cascade = CascadeType.ALL)
    List<Comment> comments;


}
