package com.krdbootcamp.instagram.post;


import org.springframework.data.domain.Page;

import java.util.List;


public interface IPostService {

    Post save(Post post);
    Post update(Post post);
    void delete(Long id);
    Page<Post> findAll(Integer page, Integer size);
    Post findById(Long id);

    //fo vaadin test
    List<Post> getAll();


}
