package com.krdbootcamp.instagram.post;

import com.krdbootcamp.instagram.common.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PostDTO extends BaseDTO {


 @ApiModelProperty(required = true,hidden = false)
 private String title;

 @ApiModelProperty(required = true,hidden = false)
 private String postLink;

 @ApiModelProperty(required = false,hidden = false)
 private String description;


}
