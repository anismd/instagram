package com.krdbootcamp.instagram.post;

import org.springframework.data.repository.PagingAndSortingRepository;




public interface PostRepository extends PagingAndSortingRepository<Post,Long>  {

}
