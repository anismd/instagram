package com.krdbootcamp.instagram.follower;

import com.krdbootcamp.instagram.common.exception.NotFoundException;
import com.krdbootcamp.instagram.following.Following;
import com.krdbootcamp.instagram.user.IUserService;
import com.krdbootcamp.instagram.user.User;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FollowerService implements IFollowerService {
    private FollowerRepository repository;
    private IUserService iUserService;

    @Override
    public Follower save(Follower follower) {
        Long userId=follower.getUser().getId();
        User user= iUserService.findById(userId);
        follower.setUser(user);
        return  repository.save(follower);
    }

    @Override
    public Follower saveVaadin(Follower follower) {
        return repository.save(follower);
    }

    @Override
    public Follower update(Follower follower) {
        Follower lastFollower= findById(follower.getId());
        lastFollower.setUserName(follower.getUserName());
        lastFollower.setProfileImage(follower.getProfileImage());
        lastFollower.setIsFollower(follower.getIsFollower());
        Long userId=follower.getUser().getId();
        User user= iUserService.findById(userId);
        lastFollower.setUser(user);
        return repository.save(lastFollower);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);

    }

    @Override
    public Page<Follower> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page,size));
    }

    @Override
    public Page<Follower> findAllByUser(Integer page, Integer size, Long userId) {
        return  repository.findAllByUserId(PageRequest.of(page,size),userId);
    }

    @Override
    public Follower findById(Long id) {
        Optional<Follower> optionalFollower=repository.findById(id);
        if (!optionalFollower.isPresent()){
            throw  new NotFoundException("Not Found Follower");
        }
        return optionalFollower.get();
    }


    //for vaadin Test
    @Override
    public List<Follower> getAll() {
            return (List<Follower>) repository.findAll();

    }
}
