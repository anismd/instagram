package com.krdbootcamp.instagram.follower;

import com.krdbootcamp.instagram.comment.Comment;
import com.krdbootcamp.instagram.comment.CommentDTO;
import com.krdbootcamp.instagram.common.PagingData;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api-followerController")
@AllArgsConstructor
public class FollowerController {

    private final IFollowerService iFollowerService;
    private final FollowerMapper mapper;


    @PostMapping
    public ResponseEntity<FollowerDTO> save(@RequestBody FollowerDTO followerDTO){
        Follower follower=mapper.toFollower(followerDTO);
        iFollowerService.save(follower);
        return  ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/v2")
    public ResponseEntity<FollowerDTO> saveVaadin(@RequestBody FollowerDTO followerDTO){
        Follower follower=mapper.toFollower(followerDTO);
        iFollowerService.saveVaadin(follower);
        return  ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    public ResponseEntity<FollowerDTO> update(@RequestBody FollowerDTO followerDTO){
        Follower follower=mapper.toFollower(followerDTO);
        iFollowerService.update(follower);
        return ResponseEntity.ok(followerDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        iFollowerService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<PagingData<FollowerDTO>> getAll(@PathVariable Integer page, @PathVariable Integer size){
        Page<Follower> followerPage=iFollowerService.findAll(page,size);
        int total=followerPage.getTotalPages();
        List<Follower> followers=followerPage.getContent();
        List<FollowerDTO> followerDTOS=mapper.toFollowerDTOS(followers);
        PagingData<FollowerDTO> followerDTOPagingData=new PagingData<>(total,page,followerDTOS);
        return ResponseEntity.ok(followerDTOPagingData);
    }

    @GetMapping("/find-by-user-id/{page}/{size}/{userId}")
        public ResponseEntity<PagingData<FollowerDTO>> findAllByUser(@PathVariable Integer page, @PathVariable Integer size,@PathVariable Long userId){
            Page<Follower> followerPage=iFollowerService.findAllByUser(page,size,userId);
            int total=followerPage.getTotalPages();
            List<Follower> followers=followerPage.getContent();
            List<FollowerDTO> followerDTOS=mapper.toFollowerDTOS(followers);
            PagingData<FollowerDTO> followerDTOPagingData=new PagingData<>(total,page,followerDTOS);
            return ResponseEntity.ok(followerDTOPagingData);
    }


    @GetMapping("/{id}")
    public ResponseEntity<FollowerDTO> getById(@PathVariable Long id){
        Follower follower=iFollowerService.findById(id);
        FollowerDTO followerDTO=mapper.toFollowerDTO(follower);
        return ResponseEntity.ok(followerDTO);
    }


    //for vaadin test
    @GetMapping
    public  ResponseEntity<List<FollowerDTO>> getAll(){
        List<Follower> followerList=iFollowerService.getAll();
        List<FollowerDTO> followerDTOS=mapper.toFollowerDTOS(followerList);
        return ResponseEntity.ok(followerDTOS);
    }

}
