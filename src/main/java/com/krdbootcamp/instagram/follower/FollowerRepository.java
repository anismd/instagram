package com.krdbootcamp.instagram.follower;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FollowerRepository extends PagingAndSortingRepository<Follower,Long> {
    Page<Follower> findAllByUserId(Pageable pageable,Long userId);
    Page<Follower> findAll(Pageable pageable);


}
