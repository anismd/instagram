package com.krdbootcamp.instagram.follower;

import org.springframework.data.domain.Page;

import java.util.List;

public interface IFollowerService {
    Follower save(Follower follower);
    Follower saveVaadin(Follower follower);
    Follower update(Follower follower);
    void delete (Long id);
    Page<Follower>findAll(Integer page, Integer size);
    Page<Follower> findAllByUser(Integer page, Integer size, Long userId);
    Follower findById(Long id);

    //for vaadin Test
    List<Follower> getAll();
}
