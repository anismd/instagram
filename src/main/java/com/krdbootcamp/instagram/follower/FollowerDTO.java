package com.krdbootcamp.instagram.follower;

import com.krdbootcamp.instagram.common.BaseDTO;
import com.krdbootcamp.instagram.user.UserDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FollowerDTO extends BaseDTO {

    @ApiModelProperty(required = true,hidden = false)
    private String userName;

    @ApiModelProperty(required = true,hidden = false)
    private String profile;

    @ApiModelProperty(required = true,hidden = false)
    private Boolean isFollower;

    @ApiModelProperty(required = true,hidden = false)
    private UserDTO user;
}
