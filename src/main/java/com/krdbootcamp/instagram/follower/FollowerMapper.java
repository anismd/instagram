package com.krdbootcamp.instagram.follower;

import com.krdbootcamp.instagram.like.Like;
import com.krdbootcamp.instagram.like.LikeDTO;
import com.krdbootcamp.instagram.user.User;
import com.krdbootcamp.instagram.user.UserMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring", uses = {UserMapper.class})
public interface FollowerMapper {

    Follower toFollower(FollowerDTO followerDTO);
    FollowerDTO toFollowerDTO(Follower follower);
    List<Follower> toFollowers(List<FollowerDTO> followerDTOS);
    List<FollowerDTO> toFollowerDTOS(List<Follower> followers);

}
