package com.krdbootcamp.instagram.follower;

import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Audited
@Table(name = "tbl_follower")
@Entity
public class Follower extends BaseEntity {

    @NotNull
    @Column(name = "user_name")
    private String userName;

    @NotNull
    @Column(name = "profile_image")
    private String profileImage;

    @NotNull
    @Column(name = "is_follower")
    private Boolean isFollower;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
