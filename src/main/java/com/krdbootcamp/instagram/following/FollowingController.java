package com.krdbootcamp.instagram.following;

import com.krdbootcamp.instagram.common.PagingData;
import com.krdbootcamp.instagram.follower.Follower;
import com.krdbootcamp.instagram.follower.FollowerDTO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/api-followingController")
@AllArgsConstructor
public class FollowingController {
    private final IFollowingService iFollowingService;
    private final FollowingMapper mapper;


    @PostMapping
    public ResponseEntity<FollowingDTO> save(@RequestBody FollowingDTO followingDTO) {
        Following following = mapper.toFollowing(followingDTO);
        iFollowingService.save(following);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/v2")
    public ResponseEntity<FollowingDTO> saveVaadin(@RequestBody FollowingDTO followingDTO) {
        Following following = mapper.toFollowing(followingDTO);
        iFollowingService.saveVaadin(following);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    public ResponseEntity<FollowingDTO> update(@RequestBody FollowingDTO followingDTO) {
        Following following = mapper.toFollowing(followingDTO);
        iFollowingService.update(following);
        return ResponseEntity.ok(followingDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        iFollowingService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<PagingData<FollowingDTO>> getAll(@PathVariable Integer page, @PathVariable Integer size) {
        Page<Following> followerPage = iFollowingService.findAll(page, size);
        int total = followerPage.getTotalPages();
        List<Following> followings = followerPage.getContent();
        List<FollowingDTO> followingDTOS = mapper.toFollowingDTOS(followings);
        PagingData<FollowingDTO> followerDTOPagingData = new PagingData<>(total, page, followingDTOS);
        return ResponseEntity.ok(followerDTOPagingData);
    }

    @GetMapping("/find-by-user-id/{page}/{size}/{userId}")
    public ResponseEntity<PagingData<FollowingDTO>> findAllByUser(@PathVariable Integer page, @PathVariable Integer size, @PathVariable Long userId) {
        Page<Following> followingPage = iFollowingService.findAllByUser(page, size, userId);
        int total = followingPage.getTotalPages();
        List<Following> followings = followingPage.getContent();
        List<FollowingDTO> followingDTOS = mapper.toFollowingDTOS(followings);
        PagingData<FollowingDTO> followerDTOPagingData = new PagingData<>(total, page, followingDTOS);
        return ResponseEntity.ok(followerDTOPagingData);
    }



    //for vaadin test
    @GetMapping
    public  ResponseEntity<List<FollowingDTO>> getAll(){
        List<Following> followerList=iFollowingService.getAll();
        List<FollowingDTO> followingDTOS=mapper.toFollowingDTOS(followerList);
        return ResponseEntity.ok(followingDTOS);
    }
}