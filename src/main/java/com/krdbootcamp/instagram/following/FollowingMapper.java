package com.krdbootcamp.instagram.following;

import com.krdbootcamp.instagram.user.User;
import com.krdbootcamp.instagram.user.UserMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring", uses = {UserMapper.class})
public interface FollowingMapper {

    Following toFollowing(FollowingDTO followingDTO);
    FollowingDTO toFollowingDTO(Following following);
    List<Following> toFollowings(List<FollowingDTO> followingDTOS);
    List<FollowingDTO> toFollowingDTOS(List<Following> followings);
}
