package com.krdbootcamp.instagram.following;

import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Audited
@Table(name = "tbl_following")
@Entity
public class Following extends BaseEntity {

    @NotNull
    @Column(name = "user_name")
    private String userName;

    @NotNull
    @Column(name = "profile_image")
    private String profileImage;

    @NotNull
    @Column(name = "remove_from_following")
    private Boolean removeFromFollowing;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
