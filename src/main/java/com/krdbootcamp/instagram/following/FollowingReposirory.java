package com.krdbootcamp.instagram.following;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FollowingReposirory extends PagingAndSortingRepository<Following,Long> {
    Page<Following> findAllByUserId(Pageable pageable, Long userId);
    Page<Following> findAll(Pageable pageable);



}
