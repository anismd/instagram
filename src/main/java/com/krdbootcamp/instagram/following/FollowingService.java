package com.krdbootcamp.instagram.following;

import com.krdbootcamp.instagram.common.exception.NotFoundException;


import com.krdbootcamp.instagram.user.IUserService;
import com.krdbootcamp.instagram.user.User;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FollowingService implements IFollowingService {
    private FollowingReposirory repository;
    private IUserService iUserService;

    @Override
    public Following save(Following following) {
        Long userId = following.getUser().getId();
        User user = iUserService.findById(userId);
        following.setUser(user);
        return repository.save(following);
    }

    @Override
    public Following saveVaadin(Following following) {
        return repository.save(following);
    }

    @Override
    public Following update(Following following) {
        Following lastSaveFollowing=findById(following.getId());
        lastSaveFollowing.setUserName(following.getUserName());
        lastSaveFollowing.setProfileImage(following.getProfileImage());
        lastSaveFollowing.setRemoveFromFollowing(following.getRemoveFromFollowing());
        Long userId = following.getUser().getId();
        User user = iUserService.findById(userId);
        lastSaveFollowing.setUser(user);
        return repository.save(lastSaveFollowing);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);

    }

    @Override
    public Page<Following> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Page<Following> findAllByUser(Integer page, Integer size, Long userId) {
        return repository.findAllByUserId(PageRequest.of(page, size), userId);
    }

    @Override
    public Following findById(Long id) {
        Optional<Following> optionalFollower = repository.findById(id);
        if (!optionalFollower.isPresent()) {
            throw new NotFoundException("Not Found Following");
        }
        return optionalFollower.get();
    }

    //forvaadin test
    @Override
    public List<Following> getAll() {
            return (List<Following>) repository.findAll();
    }
}