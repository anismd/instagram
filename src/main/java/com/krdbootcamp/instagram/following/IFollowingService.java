package com.krdbootcamp.instagram.following;

import org.springframework.data.domain.Page;

import java.util.List;

public interface IFollowingService {
    Following save(Following following);
    Following saveVaadin(Following following);
    Following update(Following following);
    void delete (Long id);
    Page<Following> findAll(Integer page, Integer size);
    Page<Following> findAllByUser(Integer page, Integer size, Long userId);
    Following findById(Long id);

    //for vaadin test
    List<Following> getAll();
}
