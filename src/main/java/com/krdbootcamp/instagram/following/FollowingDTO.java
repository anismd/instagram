package com.krdbootcamp.instagram.following;

import com.krdbootcamp.instagram.common.BaseDTO;
import com.krdbootcamp.instagram.user.UserDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FollowingDTO extends BaseDTO {
    @ApiModelProperty(required = true,hidden = false)
    private String userName;

    @ApiModelProperty(required = true,hidden = false)
    private String profileImage;

    @ApiModelProperty(required = true,hidden = false)
    private Boolean removeFromFollowing;

    @ApiModelProperty(required = true,hidden = false)
    private UserDTO user;
}
