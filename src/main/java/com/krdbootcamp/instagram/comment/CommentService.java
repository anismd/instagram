package com.krdbootcamp.instagram.comment;

import com.krdbootcamp.instagram.common.exception.NotFoundException;
import com.krdbootcamp.instagram.following.Following;
import com.krdbootcamp.instagram.post.IPostService;
import com.krdbootcamp.instagram.post.Post;
import com.krdbootcamp.instagram.user.IUserService;
import com.krdbootcamp.instagram.user.User;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CommentService  implements ICommentService{

    private CommentRepository repository;

    private IUserService iUserService;

    private IPostService iPostService;

    @Override
    public Comment save(Comment comment) {
        Long userId=comment.getUser().getId();
        Long postId=comment.getPost().getId();
        User user=iUserService.findById(userId);
        Post post=iPostService.findById(postId);
        comment.setUser(user);
        comment.setPost(post);
        return repository.save(comment);
    }

    @Override
    public Comment saveVaadin(Comment comment) {
        return repository.save(comment);
    }

    @Override
    public Comment update(Comment comment) {
        Comment lastComment= findById(comment.getId());
        lastComment.setDate(comment.getDate());
        lastComment.setMessage(comment.getMessage());
        Long userId=comment.getUser().getId();
        Long postId=comment.getPost().getId();
        User user=iUserService.findById(userId);
        Post post=iPostService.findById(postId);
        lastComment.setUser(user);
        lastComment.setPost(post);
        return repository.save(lastComment);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);
    }

    @Override
    public Page<Comment> findByUser(Integer page, Integer size, Long userId) {
        return repository.findAllByUser_Id(PageRequest.of(page,size),userId);
    }

    @Override
    public Page<Comment> findByPost(Integer page, Integer size, Long postId) {
        return repository.findAllByPost_Id(PageRequest.of(page,size),postId);
    }


    @Override
    public Comment findById(Long id) {
        Optional<Comment> commentOptional=repository.findById(id);
        if(!commentOptional.isPresent()){
            throw new NotFoundException("NOT Found Comment");
        }
        return commentOptional.get();
    }

    @Override
    public Page<Comment> findAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page,size));
    }


    //for vaadin test
    @Override
    public List<Comment> getAll() {
            return (List<Comment>) repository.findAll();
        }
}