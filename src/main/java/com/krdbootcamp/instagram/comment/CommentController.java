package com.krdbootcamp.instagram.comment;

import com.krdbootcamp.instagram.common.PagingData;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api-commentController")
@AllArgsConstructor
public class CommentController {
    private final CommentMapper mapper;
    private final ICommentService iCommentService;

    @PostMapping
    public ResponseEntity<CommentDTO> save(@RequestBody CommentDTO commentDTO){
        Comment comment=mapper.toComment(commentDTO);
        iCommentService.save(comment);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/v2")
    public ResponseEntity<CommentDTO> saveVaadin(@RequestBody CommentDTO commentDTO){
        Comment comment=mapper.toComment(commentDTO);
        iCommentService.saveVaadin(comment);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping
    public ResponseEntity<CommentDTO> update(@RequestBody CommentDTO commentDTO){
        Comment comment=mapper.toComment(commentDTO);
        iCommentService.update(comment);
        return ResponseEntity.ok(commentDTO);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        iCommentService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommentDTO> findById(@PathVariable Long id){
        Comment comment=iCommentService.findById(id);
        CommentDTO commentDTO=mapper.toCommentDTO(comment);
        return ResponseEntity.ok(commentDTO);
    }


    @GetMapping("/all-by-user-id/{page}/{size}/{userId}")
    public ResponseEntity<PagingData<CommentDTO>> getAllByUserId(@PathVariable Integer page, @PathVariable Integer size, @PathVariable Long userId){
        Page<Comment> commentPage=iCommentService.findByUser(page,size, userId);
        int total=commentPage.getTotalPages();
        List<Comment> comments=commentPage.getContent();
        List<CommentDTO> commentDTOS=mapper.toCommentDTOS(comments);
        PagingData<CommentDTO> commentDTOPagingData=new PagingData<>(total,page,commentDTOS);
        return ResponseEntity.ok(commentDTOPagingData);
    }

    @GetMapping("/all-by-post-id/{page}/{size}/{postId}")
    public ResponseEntity<PagingData<CommentDTO>> getAllByPostId(@PathVariable Integer page, @PathVariable Integer size,@PathVariable Long postId){
        Page<Comment> commentPage=iCommentService.findByPost(page,size, postId);
        int total=commentPage.getTotalPages();
        List<Comment> comments=commentPage.getContent();
        List<CommentDTO> commentDTOS=mapper.toCommentDTOS(comments);
        PagingData<CommentDTO> commentDTOPagingData=new PagingData<>(total,page,commentDTOS);
        return ResponseEntity.ok(commentDTOPagingData);
    }


    // for vaadin test
    @GetMapping
    public  ResponseEntity<List<CommentDTO>> getAll(){
        List<Comment> commentList=iCommentService.getAll();
        List<CommentDTO> commentDTOS=mapper.toCommentDTOS(commentList);
        return ResponseEntity.ok(commentDTOS);
    }


}
