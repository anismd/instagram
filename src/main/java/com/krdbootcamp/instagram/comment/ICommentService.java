package com.krdbootcamp.instagram.comment;

import org.springframework.data.domain.Page;

import java.util.List;

public interface ICommentService {

    Comment save(Comment comment);
    Comment saveVaadin(Comment comment);

    Comment update(Comment comment);

    void delete(Long id);

    Page<Comment> findByUser(Integer page, Integer size, Long userId);

    Page<Comment> findByPost(Integer page, Integer size, Long postId);

    Comment findById(Long id);

    Page<Comment> findAll(Integer page,Integer size);

    // for vaading Test
    List<Comment> getAll ();
}
