package com.krdbootcamp.instagram.comment;

import com.krdbootcamp.instagram.post.PostMapper;
import com.krdbootcamp.instagram.user.UserMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring", uses = {PostMapper.class, UserMapper.class})
public interface CommentMapper {


    Comment toComment(CommentDTO commentDTO);

    CommentDTO toCommentDTO(Comment comment);

    List<Comment> toComments(List<CommentDTO> commentDTOS);

    List<CommentDTO> toCommentDTOS(List<Comment> comments);
}

