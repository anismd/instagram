package com.krdbootcamp.instagram.comment;

import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.post.Post;
import com.krdbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity
@Table(name = "tbl_comment")
@Data
@Audited
public class Comment extends BaseEntity {

    @NotNull
    @Column(name = "date")
    private Date date;


    @NotNull
    @Column(name = "message")
    private String message;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;


}