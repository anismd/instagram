package com.krdbootcamp.instagram.comment;

import com.krdbootcamp.instagram.common.BaseDTO;
import com.krdbootcamp.instagram.post.PostDTO;
import com.krdbootcamp.instagram.user.UserDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;


@Data
public class CommentDTO extends BaseDTO {

    @ApiModelProperty(required = true,hidden = false)
    private Date date;

    @ApiModelProperty(required = true,hidden = false)
    private String message;

    @ApiModelProperty(required = true,hidden = false)
    private UserDTO user;

    @ApiModelProperty(required = true,hidden = false)
    private PostDTO post;


}

