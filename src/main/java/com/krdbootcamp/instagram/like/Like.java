package com.krdbootcamp.instagram.like;

import com.krdbootcamp.instagram.comment.Comment;
import com.krdbootcamp.instagram.common.BaseEntity;
import com.krdbootcamp.instagram.post.Post;
import com.krdbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Audited
@Table(name = "tbl_like")
@Entity
public class Like extends BaseEntity {

    @NotNull
    @Column(name = "is_favorite")
    private Boolean isFavorite;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;




}
