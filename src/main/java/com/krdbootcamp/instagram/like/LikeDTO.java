package com.krdbootcamp.instagram.like;
import com.krdbootcamp.instagram.common.BaseDTO;
import com.krdbootcamp.instagram.post.PostDTO;
import com.krdbootcamp.instagram.user.UserDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class LikeDTO extends BaseDTO {

    @ApiModelProperty(required = true, hidden = false)
    private Boolean isFavorite;

    @ApiModelProperty(required = true, hidden = false)
    private PostDTO post;

    @ApiModelProperty(required = true, hidden = false)
    private UserDTO user;

}
