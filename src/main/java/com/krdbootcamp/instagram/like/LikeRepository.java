package com.krdbootcamp.instagram.like;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface LikeRepository extends PagingAndSortingRepository<Like, Long>, JpaSpecificationExecutor<Like> {


    Page<Like> findAllByUser_Id(Pageable pageable,Long userAppId);
    Page<Like> findAllByPost_Id(Pageable pageable,Long postId);
    Page<Like> findAll(Pageable pageable);


}
