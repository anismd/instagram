package com.krdbootcamp.instagram.like;

import com.krdbootcamp.instagram.common.exception.NotFoundException;
import com.krdbootcamp.instagram.post.IPostService;
import com.krdbootcamp.instagram.post.Post;
import com.krdbootcamp.instagram.user.IUserService;
import com.krdbootcamp.instagram.user.User;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class LikeService implements ILikeService{

    private final LikeRepository repository;
    private final IPostService iPostService;
    private final IUserService iUserService;

    @Override
    public Like save(Like like) {
        Long userId= like.getUser().getId();
        Long postId= like.getPost().getId();
        User user=iUserService.findById(userId);
        Post post= iPostService.findById(postId);
        like.setUser(user);
        like.setPost(post);
        return repository.save(like);
    }

    @Override
    public Like saveVaadin(Like like) {
        return repository.save(like);
    }

    @Override
    public Like update(Like like) {
        Like lastLike= findById(like.getId());
        lastLike.setIsFavorite(like.getIsFavorite());
        Long userId= like.getUser().getId();
        Long postId= like.getPost().getId();
        User user=iUserService.findById(userId);
        Post post= iPostService.findById(postId);
        lastLike.setUser(user);
        lastLike.setPost(post);
        return repository.save(lastLike);
    }

    @Override
    public void delete(Long id) {
        findById(id);
        repository.deleteById(id);
    }


    @Override
    public Page<Like> getAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page,size));
    }

    @Override
    public Page<Like> findByPost(Integer page, Integer size, Long postId) {
        return repository.findAllByPost_Id(PageRequest.of(page,size),postId);
    }

    @Override
    public Page<Like> findByUser(Integer page, Integer size, Long userId) {
        return repository.findAllByUser_Id(PageRequest.of(page,size),userId);
    }


    @Override
    public Like findById(Long id) {
        Optional<Like> likeOptional=repository.findById(id);
        if(!likeOptional.isPresent()){
            throw new NotFoundException("NOT Found Like");
        }
        return likeOptional.get();
    }


    //for vaadin test
    @Override
    public List<Like> getAll() {
        return (List<Like>) repository.findAll();
    }
}