package com.krdbootcamp.instagram.like;

import com.krdbootcamp.instagram.common.PagingData;
import com.krdbootcamp.instagram.follower.Follower;
import com.krdbootcamp.instagram.follower.FollowerDTO;
import com.krdbootcamp.instagram.following.Following;
import com.krdbootcamp.instagram.following.FollowingDTO;
import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api-likeController")
@AllArgsConstructor
public class LikeController {
    private final ILikeService iLikeService;
    private final LikeMapper mapper;


    @PostMapping
    public ResponseEntity<LikeDTO> save(@RequestBody LikeDTO likeDTO){
        Like like=mapper.toLike(likeDTO);
        iLikeService.save(like);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/v2")
    public ResponseEntity<LikeDTO> saveVaadin(@RequestBody LikeDTO likeDTO){
        Like like=mapper.toLike(likeDTO);
        iLikeService.saveVaadin(like);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        iLikeService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<LikeDTO> findById(@PathVariable Long id){
        Like like=iLikeService.findById(id);
        LikeDTO likeDTO=mapper.toLikeDTO(like);
        return ResponseEntity.ok(likeDTO);
    }

    @PutMapping
    public ResponseEntity<LikeDTO> update(@RequestBody LikeDTO likeDTO){
        Like like=mapper.toLike(likeDTO);
        iLikeService.update(like);
        return ResponseEntity.ok(likeDTO);
    }


    @Timed("Like.getAll")
    @GetMapping("/v1/{page}/{size}")
    public ResponseEntity<PagingData<LikeDTO>> getAll(@PathVariable Integer page, @PathVariable Integer size){
        Page<Like> likePage=iLikeService.getAll(page,size);
        int total=likePage.getTotalPages();
        List<Like> likes=likePage.getContent();
        List<LikeDTO> likeDTOS=mapper.toLikeDTOS(likes);
        PagingData<LikeDTO> likeDTOPagingData=new PagingData<>(total,page,likeDTOS);
        return ResponseEntity.ok(likeDTOPagingData);
    }


    @GetMapping("/v1/filter-by-user/{page}/{size}/{userAppId}")
    public ResponseEntity<PagingData<LikeDTO>> getByUsers(@PathVariable Integer page, @PathVariable Integer size,@PathVariable Long userAppId){
        Page<Like> likePage=iLikeService.findByUser(page,size,userAppId);
        int total=likePage.getTotalPages();
        List<Like> likes=likePage.getContent();
        List<LikeDTO> likeDTOS=mapper.toLikeDTOS(likes);
        PagingData<LikeDTO> likeDTOPagingData=new PagingData<>(total,page,likeDTOS);
        return ResponseEntity.ok(likeDTOPagingData);
    }

    @GetMapping("/all-by-post-id/{page}/{size}/{postId}")
    public ResponseEntity<PagingData<LikeDTO>> getAllByPostId(@PathVariable Integer page, @PathVariable Integer size,@PathVariable Long postId){
        Page<Like> likePage=iLikeService.findByPost(page,size, postId);
        int total=likePage.getTotalPages();
        List<Like> likes=likePage.getContent();
        List<LikeDTO> likeDTOS=mapper.toLikeDTOS(likes);
        PagingData<LikeDTO> likeDTOPagingData=new PagingData<>(total,page,likeDTOS);
        return ResponseEntity.ok(likeDTOPagingData);
    }


    //for vaadin test
    @GetMapping
    public  ResponseEntity<List<LikeDTO>> getAll(){
        List<Like> likeList=iLikeService.getAll();
        List<LikeDTO> likeDTOS=mapper.toLikeDTOS(likeList);
        return ResponseEntity.ok(likeDTOS);
    }
}
