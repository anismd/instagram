package com.krdbootcamp.instagram.like;

import com.krdbootcamp.instagram.post.PostMapper;
import com.krdbootcamp.instagram.user.UserMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring", uses = {PostMapper.class, UserMapper.class})
public interface LikeMapper {

    Like toLike(LikeDTO likeDTO);
    LikeDTO toLikeDTO(Like like);
    List<Like> toLikes(List<LikeDTO> likeDTOS);
    List<LikeDTO> toLikeDTOS(List<Like> likes);
}
