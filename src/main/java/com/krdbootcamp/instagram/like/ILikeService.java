package com.krdbootcamp.instagram.like;

import org.springframework.data.domain.Page;

import java.util.List;


public interface ILikeService {
    Like save(Like like);
    Like saveVaadin(Like like);

    Like update(Like like);

    void delete(Long id);

    Page<Like> getAll(Integer page,Integer size);

    Page<Like> findByPost(Integer page,Integer size,Long postId);

    Page<Like> findByUser(Integer page,Integer size,Long userId);

    Like findById(Long id);

    //for vaadin test
    List<Like> getAll();

}
